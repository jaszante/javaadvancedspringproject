package nl.inholland.example.modal;

import javax.persistence.*;

@Entity
public class Address {
    @Id
    @GeneratedValue
    private Long id;
    private String streetName;
    private int houseNR;
    private String zipCode;
    @ManyToOne
    private Person person;

    public Address() {
    }

    public Address(String streetName, int houseNR, String zipCode) {
        this.streetName = streetName;
        this.houseNR = houseNR;
        this.zipCode = zipCode;
    }

    public Address(String streetName, int houseNR, String zipCode, Person person) {
        this.streetName = streetName;
        this.houseNR = houseNR;
        this.zipCode = zipCode;
        this.person = person;
    }

    public Long getId() {
        return id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getHouseNR() {
        return houseNR;
    }

    public void setHouseNR(int houseNR) {
        this.houseNR = houseNR;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
