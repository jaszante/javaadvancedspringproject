package nl.inholland.example.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.inholland.example.modal.Address;
import nl.inholland.example.modal.Person;
import nl.inholland.example.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("person")
public class PersonController {

    @Autowired
    private PersonService service;

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllPersons() {
        return ResponseEntity.status(200).body(service.getAllPersons());
    }
    @RequestMapping( method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPerson(@RequestBody int id) {
        return ResponseEntity.status(200).body(service.getPerson(id));
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPerson(@RequestBody Person newPerson) {
        return ResponseEntity.status(200).body(service.addPerson(newPerson));
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePerson(@RequestBody Person changePerson) {
        return ResponseEntity.status(200).body(service.updatePerson(changePerson));
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deletePerson(@RequestBody int id){
        service.deletePerson(id);
        return ResponseEntity.status(200).body("Successfully deleted");
    }
    @RequestMapping(value = "/addresses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllAddressesForUser(@RequestBody int id){
        return ResponseEntity.status(200).body(service.getAllAddressesForUser(id));
    }

    @RequestMapping(value = "/addAddress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAddress(@RequestBody ObjectNode json) {
        int id = json.get("id").asInt();
        JsonNode jsonAddress = json.get("address");

        return ResponseEntity.status(200).body(service.addAddress(id, jsonAddress));
    }

}
