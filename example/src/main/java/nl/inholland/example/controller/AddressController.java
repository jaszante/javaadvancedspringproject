package nl.inholland.example.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.inholland.example.modal.Address;
import nl.inholland.example.modal.Person;
import nl.inholland.example.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllAddresses() {
        return ResponseEntity.status(200).body(addressService.getAllAddresses());
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAddress(@RequestBody ObjectNode json) {
         int id = json.get("id").asInt();
         JsonNode jsonAddress = json.get("address");
         Address address = new Address(jsonAddress.get("streetName").asText(),jsonAddress.get("houseNR").asInt(), jsonAddress.get("zipCode").asText());
         Address returnAddress = addressService.addAddress(id, address);
        return ResponseEntity.status(200).body(returnAddress);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePerson(@RequestBody Person changePerson) {
        return ResponseEntity.status(200).body("kaas");
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteAddress(@RequestBody int id) {
        addressService.deleteAddress(id);
        return ResponseEntity.status(200).body("Successfully deleted");
    }
}
