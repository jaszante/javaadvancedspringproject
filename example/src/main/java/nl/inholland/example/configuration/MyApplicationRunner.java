package nl.inholland.example.configuration;

import nl.inholland.example.modal.Address;
import nl.inholland.example.modal.Person;
import nl.inholland.example.repository.AddressRepository;
import nl.inholland.example.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class MyApplicationRunner implements ApplicationRunner {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Person person1 = new Person("kaas", "kop", 8);
        Person person2 = new Person("kees", "zonderkop", 8);
        personRepository.save(person1);
        personRepository.save(person2);
        Address address1 = new Address("Karel Straat", 45, "1234BE", person1);
        Address address2 = new Address("keizersgracht", 2, "4321BA", person2);
        addressRepository.save(address1);
        addressRepository.save(address2);
    }
}
