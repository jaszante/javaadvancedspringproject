package nl.inholland.example.service;

import com.fasterxml.jackson.databind.JsonNode;
import nl.inholland.example.modal.Address;
import nl.inholland.example.modal.Person;
import nl.inholland.example.repository.AddressRepository;
import nl.inholland.example.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private AddressRepository addressRepository;

    private int id = 3;

    public PersonService() {
    }

    public List<Person> getAllPersons() {
        return (List<Person>) personRepository.findAll();
    }

    public Person getPerson(long id) {
        return personRepository.findById(id).get();
    }

    public Person addPerson(Person newPerson) {
        personRepository.save(newPerson);
        return newPerson;
    }

    public Person updatePerson(Person changePerson) {
        Optional<Person> optPerson = personRepository.findById(changePerson.getId());
        if (optPerson.isPresent()) {
            Person person = optPerson.get();
            person.setFirstName(changePerson.getFirstName());
            person.setLastName(changePerson.getLastName());
            person.setAge(changePerson.getAge());
            personRepository.save(person);
        }
        return changePerson;
    }

    public void deletePerson(long id) {
        Optional<Person> optPerson = personRepository.findById(id);
        if (optPerson.isPresent()) {
            Person person = optPerson.get();
            personRepository.deleteById(person.getId());
        }
    }

    public List<Address> getAllAddressesForUser(long id) {
        Person person = personRepository.findById(id).get();
        return person.getAddresses();
    }

    public Person addAddress(long id, JsonNode jsonAddress) {

        Person person = personRepository.findById(id).get();
        Address address = new Address(jsonAddress.get("streetName").asText(),jsonAddress.get("houseNR").asInt(), jsonAddress.get("zipCode").asText(), person);
        person.getAddresses().add(address);
        personRepository.save(person);
        return person;
    }

}
