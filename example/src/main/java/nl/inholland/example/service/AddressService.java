package nl.inholland.example.service;

import nl.inholland.example.modal.Address;
import nl.inholland.example.modal.Person;
import nl.inholland.example.repository.AddressRepository;
import nl.inholland.example.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PersonRepository personRepository;

    public List<Address> getAllAddresses() {
        return (List<Address>) addressRepository.findAll();
    }

    public Address addAddress(int id, Address newAddress) {
        Optional<Person> optPerson = personRepository.findById(((long) id));
        if (optPerson.isPresent()) {
            Person person = optPerson.get();
            newAddress.setPerson(person);
            addressRepository.save(newAddress);
        }
        return newAddress;
    }

    public void deleteAddress(long id){
        addressRepository.deleteById(id);
    }
}
